#-
# ==========================================================================
# Copyright (c) 2011 Autodesk, Inc.
# All rights reserved.
#
# These coded instructions, statements, and computer programs contain
# unpublished proprietary information written by Autodesk, Inc., and are
# protected by Federal copyright law. They may not be disclosed to third
# parties or copied or duplicated in any form, in whole or in part, without
# the prior written consent of Autodesk, Inc.
# ==========================================================================
#+

ifndef INCL_BUILDRULES

#
# Include platform specific build settings
#
TOP := ..# ~/dev/src
include buildrules


#
# Always build the local plug-in when make is invoked from the
# directory.
#
all : prebuiltPlugins

endif

#
# Variable definitions
#

SRCDIR := $(TOP)/hello_maya
DSTDIR := $(TOP)/hello_maya

Hello_Maya_SOURCES  := $(TOP)/hello_maya/hello_world_cmd.cpp
Hello_Maya_OBJECTS  := $(TOP)/hello_maya/hello_world_cmd.o
Hello_Maya_PLUGIN   := $(DSTDIR)/hello_world_cmd.$(EXT)
Hello_Maya_MAKEFILE := $(DSTDIR)/Makefile
Hello_Maya_INSTALL  := ~/dev/devkit/plug-ins/plug-ins

#
# Include the optional per-plugin Makefile.inc
#
#    The file can contain macro definitions such as:
#       {pluginName}_EXTRA_CFLAGS
#       {pluginName}_EXTRA_C++FLAGS
#       {pluginName}_EXTRA_INCLUDES
#       {pluginName}_EXTRA_LIBS
-include $(SRCDIR)/Makefile.inc


#
# Set target specific flags.
#

$(Hello_Maya_OBJECTS): CFLAGS   := $(CFLAGS)   $(Hello_Maya_EXTRA_CFLAGS)
$(Hello_Maya_OBJECTS): C++FLAGS := $(C++FLAGS) $(Hello_Maya_EXTRA_C++FLAGS)
$(Hello_Maya_OBJECTS): INCLUDES := $(INCLUDES) $(Hello_Maya_EXTRA_INCLUDES)

depend_Hello_Maya:     INCLUDES := $(INCLUDES) $(Hello_Maya_EXTRA_INCLUDES)

$(Hello_Maya_PLUGIN):  LFLAGS   := $(LFLAGS) $(Hello_Maya_EXTRA_LFLAGS)
$(Hello_Maya_PLUGIN):  LIBS     := $(LIBS)   -lOpenMaya -lFoundation -lOpenMayaAnim -lOpenMayaFX -lOpenMayaRender -lOpenMayaUI $(Hello_Maya_EXTRA_LIBS)

#
# Rules definitions
#

.PHONY: depend_Hello_Maya clean_Hello_Maya Clean_Hello_Maya


$(Hello_Maya_PLUGIN): $(Hello_Maya_OBJECTS)
	-rm -f $@
	$(LD) -o $@ $(LFLAGS) $^ $(LIBS)

depend_Hello_Maya :
	makedepend $(INCLUDES) $(MDFLAGS) -f$(DSTDIR)/Makefile $(Hello_Maya_SOURCES)

clean_Hello_Maya:
	-rm -f $(Hello_Maya_OBJECTS)

Clean_Hello_Maya:
	-rm -f $(Hello_Maya_MAKEFILE).bak $(Hello_Maya_OBJECTS) $(Hello_Maya_PLUGIN)

Install_Hello_Maya:
	-cp -rf $(Hello_Maya_PLUGIN) $(Hello_Maya_INSTALL)


prebuiltPlugins: $(Hello_Maya_PLUGIN)
depend:	 depend_Hello_Maya
clean:	 clean_Hello_Maya
Clean:	 Clean_Hello_Maya
install: Install_Hello_Maya
