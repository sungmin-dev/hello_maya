#include "hello_world_cmd.h"
#include <maya/MFnPlugin.h>

void* HelloWorld::creator()
{
  return new HelloWorld;
}

MStatus initializePlugin(MObject obj)
{
  MFnPlugin plugin(obj, "Sungmin Lee", "1.0", "Any");
  MStatus status = plugin.registerCommand("helloWorld", HelloWorld::creator);
  CHECK_MSTATUS_AND_RETURN_IT(status);// what is this..?
  return status;
}

MStatus uninitializePlugin(MObject obj)
{
  MFnPlugin plugin(obj);
  MStatus status = plugin.deregisterCommand("helloWorld");
  CHECK_MSTATUS_AND_RETURN_IT(status);
  return status;
}

MStatus HelloWorld::doIt(const MArgList&)
{
  MGlobal::displayInfo("Hello, World!");
  return MS::kSuccess;
}
